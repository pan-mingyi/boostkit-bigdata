/**
 * Copyright (C) 2024-2025. Huawei Technologies Co., Ltd. All rights reserved.
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef JULIANGREGORIANREBASE_H
#define JULIANGREGORIANREBASE_H

#include <string>
#include <cstdint>
#include <vector>
#include <memory>
#include <jni.h>

namespace common {
    class JulianGregorianRebase {
    public:
        JulianGregorianRebase(std::string tz, const std::vector<int64_t> &switches, const std::vector<int64_t> &diffs);

        int64_t RebaseJulianToGregorianMicros(int64_t micros);

        std::string GetTz();

    private:
        int64_t CalculateJulianDayOffset(int64_t micros);

        std::string tz;
        std::int64_t offset{};
        std::vector<int64_t> switches;
        std::vector<int64_t> diffs;
        std::int64_t lastSwitch;
    };

    std::unique_ptr<JulianGregorianRebase> BuildJulianGregorianRebase(JNIEnv *env, jobject jsonObj);
}

#endif //JULIANGREGORIANREBASE_H
