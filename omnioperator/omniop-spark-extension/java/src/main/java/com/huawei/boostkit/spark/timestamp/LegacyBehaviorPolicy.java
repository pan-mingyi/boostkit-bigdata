package com.huawei.boostkit.spark.timestamp;

public enum LegacyBehaviorPolicy {
    EXCEPTION,
    LEGACY,
    CORRECTED
}
