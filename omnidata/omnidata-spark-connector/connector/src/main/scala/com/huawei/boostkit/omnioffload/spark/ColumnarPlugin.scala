/*
 * Copyright (C) Huawei Technologies Co., Ltd. 2021-2022. All rights reserved.
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.boostkit.omnioffload.spark

import com.huawei.boostkit.omnidata.spark.NdpConnectorUtils
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.internal.Logging
import org.apache.spark.sql.catalyst.catalog.{CatalogTable, HiveTableRelation}
import org.apache.spark.sql.catalyst.expressions.aggregate._
import org.apache.spark.sql.catalyst.expressions._
import org.apache.spark.sql.catalyst.plans.logical._
import org.apache.spark.sql.catalyst.plans.{Inner, LeftAnti, LeftOuter}
import org.apache.spark.sql.catalyst.rules.Rule
import org.apache.spark.sql.execution._
import org.apache.spark.sql.execution.aggregate.{HashAggregateExec, SortAggregateExec}
import org.apache.spark.sql.execution.command.{CreateDataSourceTableAsSelectCommand, DataWritingCommandExec}
import org.apache.spark.sql.execution.datasources.LogicalRelation
import org.apache.spark.sql.execution.exchange.ShuffleExchangeExec
import org.apache.spark.sql.execution.joins._
import org.apache.spark.sql.execution.ndp.NdpConf
import org.apache.spark.sql.execution.ndp.NdpConf.getOptimizerPushDownThreshold
import org.apache.spark.sql.hive.execution.CreateHiveTableAsSelectCommand
import org.apache.spark.sql.internal.SQLConf
import org.apache.spark.sql.types.{DataTypes, DoubleType, LongType}
import org.apache.spark.sql.{SparkSession, SparkSessionExtensions}
import java.net.URI
import scala.collection.JavaConverters

case class NdpOverrides(sparkSession: SparkSession) extends Rule[SparkPlan] {

  var numPartitions: Int = -1
  var pushDownTaskCount: Int = -1
  var isSMJ = false
  var isSort = false
  var hasCoalesce = false
  var hasShuffle = false
  var ACCURATE_QUERY_HD = "153"
  var RADIX_SORT_COLUMN_NUMS = 2

  def apply(plan: SparkPlan): SparkPlan = {
    preRuleApply(plan)
    val ruleList = Seq(CountReplaceRule)
    val afterPlan = ruleList.foldLeft(plan) { case (sp, rule) =>
      val result = rule.apply(sp)
      result
    }
    val operatorEnable = NdpConf.getNdpOperatorCombineEnabled(sparkSession)
    val optimizedPlan = if (operatorEnable) {
      replaceWithOptimizedPlan(afterPlan)
    } else {
      replaceWithOptimizedPlanNoOperator(afterPlan)
    }
    val finalPlan = replaceWithScanPlan(optimizedPlan)
    postRuleApply(finalPlan)
    finalPlan
  }

  def preRuleApply(plan: SparkPlan): Unit = {
    numPartitions = SQLConf.get.getConfString("spark.omni.sql.ndpPlugin.coalesce.numPartitions",
      NdpConnectorUtils.getNdpNumPartitionsStr("10000")).toInt
    pushDownTaskCount = NdpConnectorUtils.getPushDownTaskTotal(getOptimizerPushDownThreshold(sparkSession))
    if (CountReplaceRule.shouldReplaceCountOne(plan)) {
      pushDownTaskCount = NdpConnectorUtils.getCountTaskTotal(50)
      SQLConf.get.setConfString(SQLConf.FILES_MAX_PARTITION_BYTES.key,
        NdpConnectorUtils.getCountMaxPartSize("512MB"))
    }
    if (CountReplaceRule.shouldReplaceDistinctCount(plan)) {
      pushDownTaskCount = NdpConnectorUtils.getCountDistinctTaskTotal(50)
    }
  }

  def postRuleApply(plan: SparkPlan): Unit = {
    if (isSMJ) {
      SQLConf.get.setConfString(SQLConf.FILES_MAX_PARTITION_BYTES.key,
        NdpConnectorUtils.getSMJMaxPartSize("536870912"))
    }
  }

  //now set task total number, we can use this number pushDown task in thread
  def replaceWithScanPlan(plan: SparkPlan): SparkPlan = {
    val p = plan.transformUp {
      case scan: FileSourceScanExec =>
        scan.setRuntimePushDownSum(pushDownTaskCount)
        if (hasCoalesce && !hasShuffle) {
          // without shuffle , coalesce num is task num
          scan.setRuntimePartSum(numPartitions)
        }
        scan
      case p => p
    }
    p
  }

  def replaceWithOptimizedPlan(plan: SparkPlan): SparkPlan = {
    val p = plan.transformUp {
      case shuffle: ShuffleExchangeExec =>
        hasShuffle = true
        shuffle
      case p@ColumnarSortExec(sortOrder, global, child, testSpillFrequency) if isRadixSortExecEnable(sortOrder) =>
        isSort = true
        RadixSortExec(sortOrder, global, child, testSpillFrequency)
      case p@SortExec(sortOrder, global, child, testSpillFrequency) if isRadixSortExecEnable(sortOrder) =>
        isSort = true
        RadixSortExec(sortOrder, global, child, testSpillFrequency)
      case p@DataWritingCommandExec(cmd, child) =>
        if (isSort || isVagueAndAccurateHd(child)) {
          p
        } else {
          hasCoalesce = true
          DataWritingCommandExec(cmd, CoalesceExec(numPartitions, child))
        }
      case p@ColumnarSortMergeJoinExec(_, _, joinType, _, _, _, _, projectList)
        if joinType.equals(LeftOuter) && isTenPocJoin(p.leftKeys) && isTenPocJoin(p.rightKeys) =>
        isSMJ = true
        numPartitions = NdpConnectorUtils.getSMJNumPartitions(5000)
        ColumnarSortMergeJoinExec(leftKeys = p.leftKeys, rightKeys = p.rightKeys, joinType = LeftAnti,
          condition = p.condition, left = p.left, right = p.right, isSkewJoin = p.isSkewJoin, projectList)
      case p@SortMergeJoinExec(_, _, joinType, _, _, _, _)
        if joinType.equals(LeftOuter) && isTenPocJoin(p.leftKeys) && isTenPocJoin(p.rightKeys) =>
        isSMJ = true
        numPartitions = NdpConnectorUtils.getSMJNumPartitions(5000)
        SortMergeJoinExec(leftKeys = p.leftKeys, rightKeys = p.rightKeys, joinType = LeftAnti, condition = p.condition,
          left = p.left, right = p.right, isSkewJoin = p.isSkewJoin)
      case p@ColumnarBroadcastHashJoinExec(_, _, joinType, _, _, _, _, _, projectList) if joinType.equals(LeftOuter) && isTenPocJoin(p.leftKeys) && isTenPocJoin(p.rightKeys) =>
        ColumnarBroadcastHashJoinExec(leftKeys = p.leftKeys, rightKeys = p.rightKeys,
          joinType = LeftAnti, buildSide = p.buildSide, condition = p.condition, left = p.left,
          right = p.right, isNullAwareAntiJoin = p.isNullAwareAntiJoin, projectList)
      case p@BroadcastHashJoinExec(_, _, joinType, _, _, _, _, _) if joinType.equals(LeftOuter) && isTenPocJoin(p.leftKeys) && isTenPocJoin(p.rightKeys) =>
        BroadcastHashJoinExec(leftKeys = p.leftKeys, rightKeys = p.rightKeys, joinType = LeftAnti,
          buildSide = p.buildSide, condition = p.condition, left = p.left, right = p.right,
          isNullAwareAntiJoin = p.isNullAwareAntiJoin)
      case p@ColumnarShuffledHashJoinExec(_, _, joinType, _, _, _, _, projectList)
        if joinType.equals(LeftOuter) && isTenPocJoin(p.leftKeys) && isTenPocJoin(p.rightKeys) =>
        ColumnarShuffledHashJoinExec(p.leftKeys, p.rightKeys, LeftAnti, p.buildSide, p.condition,
          p.left, p.right, projectList)
      case p@ShuffledHashJoinExec(_, _, joinType, _, _, _, _) if joinType.equals(LeftOuter) && isTenPocJoin(p.leftKeys) && isTenPocJoin(p.rightKeys) =>
        ShuffledHashJoinExec(p.leftKeys, p.rightKeys, LeftAnti, p.buildSide, p.condition, p.left, p.right)
      case p@FilterExec(condition, child: OmniColumnarToRowExec, selectivity) =>
        val childPlan = child.transform {
          case p@OmniColumnarToRowExec(child: NdpFileSourceScanExec) =>
            ColumnarToRowExec(FileSourceScanExec(child.relation,
              child.output,
              child.requiredSchema,
              child.partitionFilters,
              child.optionalBucketSet,
              child.optionalNumCoalescedBuckets,
              child.dataFilters,
              child.tableIdentifier,
              child.partitionColumn,
              child.disableBucketedScan))
          case p@OmniColumnarToRowExec(child: FileSourceScanExec) =>
            ColumnarToRowExec(child)
          case p => p
        }
        FilterExec(condition, childPlan, selectivity)
      case c1@OmniColumnarToRowExec(c2@ColumnarFilterExec(condition, c3: FileSourceScanExec)) =>
        numPartitions = NdpConnectorUtils.getOmniColumnarNumPartitions(1000)
        if (NdpPluginEnableFlag.isAccurate(condition)) {
          pushDownTaskCount = NdpConnectorUtils.getOmniColumnarTaskCount(50)
        }
        FilterExec(condition, ColumnarToRowExec(c3))
      case p@FilterExec(condition, _, _) if NdpPluginEnableFlag.isAccurate(condition) =>
        numPartitions = NdpConnectorUtils.getFilterPartitions(1000)
        pushDownTaskCount = NdpConnectorUtils.getFilterTaskCount(50)
        p
      case p@ColumnarConditionProjectExec(projectList, condition, child)
        if condition.toString().startsWith("isnull") && (child.isInstanceOf[ColumnarSortMergeJoinExec]
          || child.isInstanceOf[ColumnarBroadcastHashJoinExec] || child.isInstanceOf[ColumnarShuffledHashJoinExec]) && isTenPocProject(projectList) =>
        ColumnarProjectExec(changeProjectList(projectList), child)
      case p@ProjectExec(projectList, filter: FilterExec)
        if filter.condition.toString().startsWith("isnull") && (filter.child.isInstanceOf[SortMergeJoinExec]
          || filter.child.isInstanceOf[BroadcastHashJoinExec] || filter.child.isInstanceOf[ShuffledHashJoinExec]) && isTenPocProject(projectList) =>
        ProjectExec(changeProjectList(projectList), filter.child)
      case p: SortAggregateExec if p.child.isInstanceOf[OmniColumnarToRowExec]
        && p.child.asInstanceOf[OmniColumnarToRowExec].child.isInstanceOf[ColumnarSortExec]
        && isAggPartial(p.aggregateAttributes) =>
        val omniColumnarToRow = p.child.asInstanceOf[OmniColumnarToRowExec]
        val omniColumnarSort = omniColumnarToRow.child.asInstanceOf[ColumnarSortExec]
        SortAggregateExec(p.requiredChildDistributionExpressions,
          p.groupingExpressions,
          p.aggregateExpressions,
          p.aggregateAttributes,
          p.initialInputBufferOffset,
          p.resultExpressions,
          SortExec(omniColumnarSort.sortOrder,
            omniColumnarSort.global,
            ColumnarToRowExec(omniColumnarSort.child),
            omniColumnarSort.testSpillFrequency))
      case p: SortAggregateExec if p.child.isInstanceOf[OmniColumnarToRowExec]
        && p.child.asInstanceOf[OmniColumnarToRowExec].child.isInstanceOf[ColumnarSortExec]
        && isAggFinal(p.aggregateAttributes) =>
        val omniColumnarToRow = p.child.asInstanceOf[OmniColumnarToRowExec]
        val omniColumnarSort = omniColumnarToRow.child.asInstanceOf[ColumnarSortExec]
        val omniShuffleExchange = omniColumnarSort.child.asInstanceOf[ColumnarShuffleExchangeExec]
        val rowToOmniColumnar = omniShuffleExchange.child.asInstanceOf[RowToOmniColumnarExec]
        SortAggregateExec(p.requiredChildDistributionExpressions,
          p.groupingExpressions,
          p.aggregateExpressions,
          p.aggregateAttributes,
          p.initialInputBufferOffset,
          p.resultExpressions,
          SortExec(omniColumnarSort.sortOrder,
            omniColumnarSort.global,
            ShuffleExchangeExec(omniShuffleExchange.outputPartitioning, rowToOmniColumnar.child,
              omniShuffleExchange.shuffleOrigin),
            omniColumnarSort.testSpillFrequency))
      case p@OmniColumnarToRowExec(agg: ColumnarHashAggregateExec)
        if agg.groupingExpressions.nonEmpty && agg.child.isInstanceOf[ColumnarShuffleExchangeExec] =>
        val omniExchange = agg.child.asInstanceOf[ColumnarShuffleExchangeExec]
        val omniHashAgg = omniExchange.child.asInstanceOf[ColumnarHashAggregateExec]
        HashAggregateExec(agg.requiredChildDistributionExpressions,
          agg.groupingExpressions,
          agg.aggregateExpressions,
          agg.aggregateAttributes,
          agg.initialInputBufferOffset,
          agg.resultExpressions,
          ShuffleExchangeExec(omniExchange.outputPartitioning,
            HashAggregateExec(omniHashAgg.requiredChildDistributionExpressions,
              omniHashAgg.groupingExpressions,
              omniHashAgg.aggregateExpressions,
              omniHashAgg.aggregateAttributes,
              omniHashAgg.initialInputBufferOffset,
              omniHashAgg.resultExpressions,
              ColumnarToRowExec(omniHashAgg.child)),
            omniExchange.shuffleOrigin))
      case p => p
    }
    p
  }

  def replaceWithOptimizedPlanNoOperator(plan: SparkPlan): SparkPlan = {
    val p = plan.transformUp {
      case shuffle: ShuffleExchangeExec =>
        hasShuffle = true
        shuffle
      case p@SortExec(sortOrder, global, child, testSpillFrequency) if isRadixSortExecEnable(sortOrder) =>
        isSort = true
        RadixSortExec(sortOrder, global, child, testSpillFrequency)
      case p@DataWritingCommandExec(cmd, child) =>
        if (isSort || isVagueAndAccurateHd(child)) {
          p
        } else {
          hasCoalesce = true
          DataWritingCommandExec(cmd, CoalesceExec(numPartitions, child))
        }
      case p@SortMergeJoinExec(_, _, joinType, _, _, _, _)
        if joinType.equals(LeftOuter) && isTenPocJoin(p.leftKeys) && isTenPocJoin(p.rightKeys) =>
        isSMJ = true
        numPartitions = NdpConnectorUtils.getSMJNumPartitions(5000)
        SortMergeJoinExec(leftKeys = p.leftKeys, rightKeys = p.rightKeys, joinType = LeftAnti, condition = p.condition,
          left = p.left, right = p.right, isSkewJoin = p.isSkewJoin)
      case p@BroadcastHashJoinExec(_, _, joinType, _, _, _, _, _) if joinType.equals(LeftOuter) && isTenPocJoin(p.leftKeys) && isTenPocJoin(p.rightKeys) =>
        BroadcastHashJoinExec(leftKeys = p.leftKeys, rightKeys = p.rightKeys, joinType = LeftAnti,
          buildSide = p.buildSide, condition = p.condition, left = p.left, right = p.right,
          isNullAwareAntiJoin = p.isNullAwareAntiJoin)
      case p@ShuffledHashJoinExec(_, _, joinType, _, _, _, _) if joinType.equals(LeftOuter) && isTenPocJoin(p.leftKeys) && isTenPocJoin(p.rightKeys) =>
        ShuffledHashJoinExec(p.leftKeys, p.rightKeys, LeftAnti, p.buildSide, p.condition, p.left, p.right)
      case p@FilterExec(condition, _, _) if NdpPluginEnableFlag.isAccurate(condition) =>
        numPartitions = NdpConnectorUtils.getFilterPartitions(1000)
        pushDownTaskCount = NdpConnectorUtils.getFilterTaskCount(50)
        p
      case p@ProjectExec(projectList, filter: FilterExec)
        if filter.condition.toString().startsWith("isnull") && (filter.child.isInstanceOf[SortMergeJoinExec]
          || filter.child.isInstanceOf[BroadcastHashJoinExec] || filter.child.isInstanceOf[ShuffledHashJoinExec]) && isTenPocProject(projectList) =>
        ProjectExec(changeProjectList(projectList), filter.child)
      case p => p
    }
    p
  }

  def isAggPartial(aggAttributes: Seq[Attribute]): Boolean = {
    aggAttributes.exists(x => x.name.equals("max") || x.name.equals("maxxx"))
  }

  def isAggFinal(aggAttributes: Seq[Attribute]): Boolean = {
    aggAttributes.exists(x => x.name.contains("avg(cast"))
  }

  def isVagueAndAccurateHd(child: SparkPlan): Boolean = {
    var result = false
    child match {
      case filter: FilterExec =>
        filter.child match {
          case columnarToRow: ColumnarToRowExec =>
            if (columnarToRow.child.isInstanceOf[FileSourceScanExec]) {
              filter.condition.foreach { x =>
                if (x.isInstanceOf[StartsWith] || x.isInstanceOf[EndsWith] || x.isInstanceOf[Contains]) {
                  result = true
                }
                x match {
                  case literal: Literal if !literal.nullable && literal.value.toString.startsWith(ACCURATE_QUERY_HD) =>
                    result = true
                  case _ =>
                }
              }
            }
          case _ =>
        }
      case _ =>
    }
    result
  }

  def changeProjectList(projectList: Seq[NamedExpression]): Seq[NamedExpression] = {
    val p = projectList.map {
      case exp: Alias =>
        Alias(Literal(null, exp.dataType), exp.name)(
          exprId = exp.exprId,
          qualifier = exp.qualifier,
          explicitMetadata = exp.explicitMetadata,
          nonInheritableMetadataKeys = exp.nonInheritableMetadataKeys
        )
      case exp => exp
    }
    p
  }

  def isRadixSortExecEnable(sortOrder: Seq[SortOrder]): Boolean = {
    sortOrder.lengthCompare(RADIX_SORT_COLUMN_NUMS) == 0 &&
      sortOrder.head.dataType == LongType &&
      sortOrder.head.child.isInstanceOf[AttributeReference] &&
      sortOrder.head.child.asInstanceOf[AttributeReference].name.startsWith("col") &&
      sortOrder(1).dataType == LongType &&
      sortOrder(1).child.isInstanceOf[AttributeReference] &&
      sortOrder(1).child.asInstanceOf[AttributeReference].name.startsWith("col") &&
      SQLConf.get.getConfString("spark.omni.sql.ndpPlugin.radixSort.enabled", "true").toBoolean
  }

  def isTenPocProject(projectList: Seq[NamedExpression]): Boolean = {
    projectList.forall {
      case exp: Alias =>
        exp.child.isInstanceOf[AttributeReference] && exp.child.asInstanceOf[AttributeReference].name.startsWith("col")
      case exp: AttributeReference =>
        exp.name.startsWith("col")
      case _ => false
    }
  }

  def isTenPocJoin(keys: Seq[Expression]): Boolean = {
    keys.forall {
      case exp: AttributeReference =>
        exp.name.startsWith("col")
      case _ => false
    }
  }
}

case class NdpRules(session: SparkSession) extends ColumnarRule with Logging {

  def ndpOverrides: NdpOverrides = NdpOverrides(session)

  override def preColumnarTransitions: Rule[SparkPlan] = plan => {
    plan
  }

  override def postColumnarTransitions: Rule[SparkPlan] = plan => {
    if (NdpPluginEnableFlag.isEnable(plan.sqlContext.sparkSession)) {
      val rule = ndpOverrides
      rule(plan)
    } else {
      plan
    }
  }

}

case class NdpOptimizerRules(session: SparkSession) extends Rule[LogicalPlan] {

  var maxSizeInBytes: BigInt = 0L

  var ACCURATE_QUERY = "000"
  val SORT_REPARTITION_PLANS: Seq[String] = Seq(
    "Sort,HiveTableRelation",
    "Sort,LogicalRelation",
    "Sort,RepartitionByExpression,HiveTableRelation",
    "Sort,RepartitionByExpression,LogicalRelation",
    "Sort,Project,HiveTableRelation",
    "Sort,Project,LogicalRelation",
    "Sort,RepartitionByExpression,Project,HiveTableRelation",
    "Sort,RepartitionByExpression,Project,LogicalRelation"
  )

  val SORT_REPARTITION_SIZE: Int = SQLConf.get.getConfString(
    "spark.omni.sql.ndpPlugin.sort.repartition.size",
    NdpConnectorUtils.getSortRepartitionSizeStr("104857600")).toInt
  val DECIMAL_PRECISION: Int = SQLConf.get.getConfString(
    "spark.omni.sql.ndpPlugin.cast.decimal.precision",
    NdpConnectorUtils.getCastDecimalPrecisionStr("15")).toInt
  val MAX_PARTITION_BYTES_ENABLE_FACTOR: Int = SQLConf.get.getConfString(
    "spark.omni.sql.ndpPlugin.max.partitionBytesEnable.factor",
    NdpConnectorUtils.getNdpMaxPtFactorStr("2")).toInt


  override def apply(plan: LogicalPlan): LogicalPlan = {
    if (NdpPluginEnableFlag.isEnable(session)) {
      val res = replaceWithOptimizedPlan(plan)
      repartition(FileSystem.get(session.sparkContext.hadoopConfiguration), plan)
      res
    } else if (NdpPluginEnableFlag.isNdpOptimizedEnable(session)) {
      applyOptimizedRules(plan)
    } else {
      plan
    }
  }

  def applyOptimizedRules(plan: LogicalPlan): LogicalPlan = {
    plan.foreach {
      case p@LogicalRelation(_, _, catalogTable, _) =>
        val sTable = catalogTable.get.identifier
        val stats = session.sessionState.catalog.getTableMetadata(sTable).stats
        if (stats.isDefined) {
          val sizeInBytes = stats.get.sizeInBytes
          if (sizeInBytes > maxSizeInBytes) {
            var fileMaxBytes = "512MB"
            var shufflePartition = "200"
            if (sizeInBytes <= 1073741824L) {
              fileMaxBytes = NdpConnectorUtils.getMixSqlBaseMaxFilePtBytesStr("256MB")
              shufflePartition = NdpConnectorUtils.getShufflePartitionsStr("200")
            } else if (sizeInBytes > 1073741824L && sizeInBytes < 1099511627776L) {
              fileMaxBytes = NdpConnectorUtils.getMixSqlBaseMaxFilePtBytesStr("256MB")
              shufflePartition = NdpConnectorUtils.getShufflePartitionsStr("1000")
            } else {
              fileMaxBytes = NdpConnectorUtils.getMixSqlBaseMaxFilePtBytesStr("128MB")
              shufflePartition = NdpConnectorUtils.getShufflePartitionsStr("1000")
            }
            SQLConf.get.setConfString(SQLConf.FILES_MAX_PARTITION_BYTES.key, fileMaxBytes)
            SQLConf.get.setConfString(SQLConf.SHUFFLE_PARTITIONS.key, shufflePartition)
            maxSizeInBytes = sizeInBytes
          }
        }
      case _ =>
    }
    plan
  }

  def replaceWithOptimizedPlan(plan: LogicalPlan): LogicalPlan = {
    plan.transformUp {
      case CreateHiveTableAsSelectCommand(tableDesc, query, outputColumnNames, mode)
        if isParquetEnable(tableDesc)
          && checkParquetFieldNames(outputColumnNames)
          && SQLConf.get.getConfString("spark.omni.sql.ndpPlugin.parquetOutput.enabled", "true")
          .toBoolean =>
        CreateDataSourceTableAsSelectCommand(
          tableDesc.copy(provider = Option("parquet")), mode, query, outputColumnNames)
      case a@Aggregate(groupingExpressions, aggregateExpressions, _)
        if SQLConf.get.getConfString("spark.omni.sql.ndpPlugin.castDecimal.enabled", "true")
          .toBoolean =>
        var ifCast = false
        if (groupingExpressions.nonEmpty && hasCount(aggregateExpressions)) {
          SQLConf.get.setConfString(SQLConf.FILES_MAX_PARTITION_BYTES.key,
            NdpConnectorUtils.getCountAggMaxFilePtBytesStr("1024MB"))
        } else if (groupingExpressions.nonEmpty && hasAvg(aggregateExpressions)) {
          SQLConf.get.setConfString(SQLConf.FILES_MAX_PARTITION_BYTES.key,
            NdpConnectorUtils.getAvgAggMaxFilePtBytesStr("256MB"))
          ifCast = true
        }
        if (ifCast) {
          a.copy(aggregateExpressions = aggregateExpressions
            .map(castSumAvgToBigInt)
            .map(_.asInstanceOf[NamedExpression]))
        }
        else {
          a
        }
      case j@Join(_, _, Inner, condition, _) =>
        // turnOffOperator()
        // 6-x-bhj
        SQLConf.get.setConfString(SQLConf.FILES_MAX_PARTITION_BYTES.key,
          NdpConnectorUtils.getBhjMaxFilePtBytesStr("512MB"))
        j
      case s@Sort(order, _, _) =>
        s.copy(order = order.map(e => e.copy(child = castStringExpressionToBigint(e.child))))
      case p => p
    }
  }

  def hasCount(aggregateExpressions: Seq[Expression]): Boolean = {
    aggregateExpressions.exists {
      case exp: Alias if (exp.child.isInstanceOf[AggregateExpression]
        && exp.child.asInstanceOf[AggregateExpression].aggregateFunction.isInstanceOf[Count]) => true
      case _ => false
    }
  }

  def hasAvg(aggregateExpressions: Seq[Expression]): Boolean = {
    aggregateExpressions.exists {
      case exp: Alias if (exp.child.isInstanceOf[AggregateExpression]
        && exp.child.asInstanceOf[AggregateExpression].aggregateFunction.isInstanceOf[Average]) => true
      case _ => false
    }
  }

  def isParquetEnable(tableDesc: CatalogTable): Boolean = {
    if (tableDesc.provider.isEmpty || tableDesc.provider.get.equals("hive")) {
      if (tableDesc.storage.outputFormat.isEmpty
        || tableDesc.storage.serde.get.equals("org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe")) {
        return true
      }
    }
    false
  }

  // ,;{}()\n\t= and space are special characters in Parquet schema
  def checkParquetFieldNames(outputColumnNames: Seq[String]): Boolean = {
    outputColumnNames.forall(!_.matches(".*[ ,;{}()\n\t=].*"))
  }

  def repartition(fs: FileSystem, plan: LogicalPlan): Unit = {
    var tables = Seq[URI]()
    var planContents = Seq[String]()
    var maxPartitionBytesEnable = true
    var existsProject = false
    var existsTable = false
    var existsAgg = false
    var existAccurate = false
    var existFilter = false
    var existJoin = false
    var existLike = false
    var isMixSql = false


    plan.foreach {
      case p@HiveTableRelation(tableMeta, _, _, _, _) =>
        if (tableMeta.storage.locationUri.isDefined) {
          tables :+= tableMeta.storage.locationUri.get
        }
        existsTable = true
        planContents :+= p.nodeName
      case p@LogicalRelation(_, _, catalogTable, _) =>
        if (catalogTable.isDefined && catalogTable.get.storage.locationUri.isDefined) {
          tables :+= catalogTable.get.storage.locationUri.get
        }
        existsTable = true
        planContents :+= p.nodeName
      case p: Project =>
        maxPartitionBytesEnable &= (p.output.length * MAX_PARTITION_BYTES_ENABLE_FACTOR < p.inputSet.size)
        existsProject = true
        planContents :+= p.nodeName
      case p: Aggregate =>
        maxPartitionBytesEnable = true
        existsProject = true
        existsAgg = true
        planContents :+= p.nodeName
      case p@Filter(condition, _) =>
        existAccurate |= NdpPluginEnableFlag.isAccurate(condition)
        existFilter = true
        existLike |= isLike(condition)
        planContents :+= p.nodeName
      case p: Join =>
        existJoin = true
        planContents :+= p.nodeName
      case p =>
        planContents :+= p.nodeName
    }

    if(!existsTable){
      return
    }

    // mix sql
    isMixSql = existJoin && existsAgg
    if (isMixSql) {
      if (existAccurate) {
        SQLConf.get.setConfString(SQLConf.SHUFFLE_PARTITIONS.key,
          NdpConnectorUtils.getAggShufflePartitionsStr("200"))
        SQLConf.get.setConfString(SQLConf.FILES_MAX_PARTITION_BYTES.key,
          NdpConnectorUtils.getMixSqlAccurateMaxFilePtBytesStr("1024MB"))
      } else {
        if (existLike) {
          SQLConf.get.setConfString(SQLConf.SHUFFLE_PARTITIONS.key,
            NdpConnectorUtils.getAggShufflePartitionsStr("200"))
        } else {
          SQLConf.get.setConfString(SQLConf.SHUFFLE_PARTITIONS.key,
            NdpConnectorUtils.getShufflePartitionsStr("5000"))
        }
        SQLConf.get.setConfString(SQLConf.FILES_MAX_PARTITION_BYTES.key,
          NdpConnectorUtils.getMixSqlBaseMaxFilePtBytesStr("128MB"))
      }
      // base sql agg shuffle partition 200 ,other 5000
    } else {
      repartitionShuffleForSort(fs, tables, planContents)
      repartitionHdfsReadForDistinct(fs, tables, plan)
      if (existJoin) {
        SQLConf.get.setConfString(SQLConf.SHUFFLE_PARTITIONS.key,
          NdpConnectorUtils.getShufflePartitionsStr("5000"))
      }
    }
  }

  def repartitionShuffleForSort(fs: FileSystem, tables: Seq[URI], planContents: Seq[String]): Unit = {
    if (!SQLConf.get.getConfString("spark.omni.sql.ndpPlugin.radixSort.enabled", "true").toBoolean) {
      return
    }

    val planContent = planContents.mkString(",")
    if (tables.length == 1
      && SORT_REPARTITION_PLANS.exists(planContent.contains(_))) {
      val partitions = Math.max(1, fs.getContentSummary(new Path(tables.head)).getLength / SORT_REPARTITION_SIZE)
      SQLConf.get.setConfString(SQLConf.SHUFFLE_PARTITIONS.key,
        NdpConnectorUtils.getSortShufflePartitionsStr("1000"))
      turnOffOperator()
    }
  }

  def repartitionHdfsReadForDistinct(fs: FileSystem, tables: Seq[URI], plan: LogicalPlan): Unit = {
    if (!SQLConf.get.getConfString("spark.omni.sql.ndpPlugin.distinct.enabled", "true").toBoolean) {
      return
    }
    if (tables.length != 1) {
      return
    }

    plan.foreach {
      case Aggregate(groupingExpressions, aggregateExpressions, _) if groupingExpressions == aggregateExpressions =>
        SQLConf.get.setConfString(SQLConf.FILES_MAX_PARTITION_BYTES.key,
          NdpConnectorUtils.getGroupMaxFilePtBytesStr("1024MB"))
        return
      case _ =>
    }
  }

  def castSumAvgToBigInt(expression: Expression): Expression = {
    val exp = expression.transform {
      case Average(cast: Cast) if cast.dataType.isInstanceOf[DoubleType]
        && cast.child.isInstanceOf[AttributeReference]
        && cast.child.asInstanceOf[AttributeReference].name.startsWith("col")=>
        Average(Cast(cast.child, DataTypes.LongType))
      case Sum(cast: Cast) if cast.dataType.isInstanceOf[DoubleType]
        && cast.child.isInstanceOf[AttributeReference]
        && cast.child.asInstanceOf[AttributeReference].name.startsWith("col")=>
        Sum(Cast(cast.child, DataTypes.LongType))
      case e =>
        e
    }
    var finalExp = exp
    exp match {
      case agg: Alias if agg.child.isInstanceOf[AggregateExpression]
        && agg.child.asInstanceOf[AggregateExpression].aggregateFunction.isInstanceOf[Sum] =>
        finalExp = Alias(Cast(agg.child, DataTypes.DoubleType), agg.name)(
          exprId = agg.exprId,
          qualifier = agg.qualifier,
          explicitMetadata = agg.explicitMetadata,
          nonInheritableMetadataKeys = agg.nonInheritableMetadataKeys
        )
      case _ =>
    }
    finalExp
  }

  def castStringExpressionToBigint(expression: Expression): Expression = {
    expression match {
      case a@AttributeReference(_, DataTypes.StringType, _, _) if a.name.startsWith("col") =>
        Cast(a, DataTypes.LongType)
      case e => e
    }
  }

  def turnOffOperator(): Unit = {
    session.sqlContext.setConf("org.apache.spark.sql.columnar.enabled", "false")
    session.sqlContext.setConf("spark.sql.join.columnar.preferShuffledHashJoin", "false")
  }

  def isLike(condition: Expression): Boolean = {
    var result = false
    condition.foreach {
      case _: StartsWith =>
        result = true
      case _ =>
    }
    result
  }
}

class ColumnarPlugin extends (SparkSessionExtensions => Unit) with Logging {
  override def apply(extensions: SparkSessionExtensions): Unit = {
    extensions.injectColumnar(session => NdpRules(session))
    extensions.injectOptimizerRule(session => NdpOptimizerRules(session))
  }
}

object NdpPluginEnableFlag {
  val ndpEnabledStr = "spark.omni.sql.ndpPlugin.enabled"
  var ACCURATE_QUERY = "000"

  def isAccurate(condition: Expression): Boolean = {
    var result = false
    condition.foreach {
      // literal need to check null
      case literal: Literal if !literal.nullable && literal.value.toString.startsWith(ACCURATE_QUERY) =>
        result = true
      case _ =>
    }
    result
  }

  val ndpOptimizedEnableStr = "spark.omni.sql.ndpPlugin.optimized.enabled"

  def isMatchedIpAddress: Boolean = {
    val ipSet = Set("xxx.xxx.xxx.xxx")
    val hostAddrSet = JavaConverters.asScalaSetConverter(NdpConnectorUtils.getIpAddress).asScala
    val res = ipSet & hostAddrSet
    res.nonEmpty
  }

  def isEnable(session: SparkSession): Boolean = {
    def ndpEnabled: Boolean = session.sqlContext.getConf(
      ndpEnabledStr, "false").trim.toBoolean
    ndpEnabled && (isMatchedIpAddress || NdpConnectorUtils.getNdpEnable)
  }

  def isEnable: Boolean = {
    def ndpEnabled: Boolean = sys.props.getOrElse(
      ndpEnabledStr, "false").trim.toBoolean
    ndpEnabled && (isMatchedIpAddress || NdpConnectorUtils.getNdpEnable)
  }
def isNdpOptimizedEnable(session: SparkSession): Boolean = {
    session.sqlContext.getConf(ndpOptimizedEnableStr, "true").trim.toBoolean
  }
}