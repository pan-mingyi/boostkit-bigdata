# OmniData Spark Connector Lib

## Building OmniData Spark Connector Lib

1. Simply run the following command from the project root directory:<br>
`mvn clean package`<br>
Then you will find jars in the "omnidata-spark-connector-lib/target/" directory.

## More Information

For further assistance, send an email to kunpengcompute@huawei.com.